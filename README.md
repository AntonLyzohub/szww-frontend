# SZWW - Frontend

The "SZWW" Project is an Internet System for Departmental Events Management. The program is divided into a backend (Spring Boot) and a frontend (Angular 11) part. The basic functionalities of the frontend part are:

- logging in, registration and password change,
- displaying the table of faculty events,
- displaying a page with details of the selected event,
- reservation of the show by the user,
- checking the reserved terms of the user,
- creating, updating, deleting departmental events, shows and appointments.
- changing user settings,
- communication with the backend via REST Api,
- adding a JWT token to the HTTP header. 

# Screenshots

![Main Page](screenshots/main-page.png)


![Event Page](screenshots/event-page.png)


![Show list](screenshots/show-list.png)


![Manage show list](screenshots/showdate-list-user.png)


![Users reservations](screenshots/zapis-norm-moje.png)
