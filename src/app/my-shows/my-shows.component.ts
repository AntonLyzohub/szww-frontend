import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ShowDate } from '../model/show-date';
import { ShowDateService } from '../service/show-date.service';
import { registerLocaleData } from '@angular/common';
import localePl from '@angular/common/locales/pl';
import { LOCALE_ID } from '@angular/core';

@Component({
  selector: 'my-shows',
  templateUrl: './my-shows.component.html',
  styleUrls: ['./my-shows.component.css'],
  providers: [{ provide: LOCALE_ID, useValue: "pl-PL" }]
})
export class MyShowsComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public events;

  constructor(private showDateService: ShowDateService, 
    private _snackBar: MatSnackBar, 
    private router: Router) { }

  ngOnInit(): void {
    registerLocaleData(localePl);
    this.getOwnShowDates();
  }

  private getOwnShowDates(): void {
    this.events = [];
    this.subscriptions.push(
      this.showDateService.getAllOwnShowDates().subscribe(showDates => {
        showDates.forEach(showDate => {
          if(this.events.length == 0)
            this.events.push({eventId: showDate.show.event.id, eventTitle: showDate.show.event.title, showDates: [showDate]});
          else {
            var eventIndex = this.events.findIndex(e => e.eventId == showDate.show.event.id);
            if(eventIndex != -1)
              this.events[eventIndex].showDates.push(showDate);
            else 
              this.events.push({eventId: showDate.show.event.id, eventTitle: showDate.show.event.title, showDates: [showDate]});
          }
        });
      })
    );
  }

  public navigateToEvent(eventId: number): void { 
    this.router.navigate(['/event/', eventId]);
  }

  public deregister(id: number): void {
    const formData = new FormData();
    formData.append('id', JSON.stringify(id));

    this.subscriptions.push(
      this.showDateService.deregisterShowDate(formData).subscribe(
        (response: ShowDate) => {
          this.getOwnShowDates();
          this._snackBar.open(`Pomyślnie wypisano Cię z pokazu "` +`${response.show.name}"`, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        },
        (errorResponse: HttpErrorResponse) => {
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
