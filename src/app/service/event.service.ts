import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CustomHttpResponse } from '../model/custom-http-response';
import { Event } from '../model/event';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  public host = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public addNewEvent(formData: FormData): Observable<Event> {
    return this.http.post<Event>(`${this.host}/event/add`, formData);
  }

  public updateEvent(id: number, formData: FormData): Observable<Event> {
    return this.http.put<Event>(`${this.host}/event/update/${id}`, formData);
  }

  public getEvent(id: number): Observable<Event> {
    return this.http.get<Event>(`${this.host}/event/find/${id}`);
  }

  public getAllEvents(categoryId: number, statusId: number): Observable<Event[]> {
    if(categoryId == null && statusId == null)
      return this.http.get<Event[]>(`${this.host}/event/list`);
    else if(categoryId == null)
      return this.http.get<Event[]>(`${this.host}/event/list/status/${statusId}`);
    else if(statusId == null)
      return this.http.get<Event[]>(`${this.host}/event/list/category/${categoryId}`);
      
    return this.http.get<Event[]>(`${this.host}/event/list/filtered/${categoryId}/${statusId}`);
  }

  public deleteEvent(id: number): Observable<CustomHttpResponse> {
    return this.http.delete<CustomHttpResponse>(`${this.host}/event/delete/${id}`);
  }
}
