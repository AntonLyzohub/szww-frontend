import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CustomHttpResponse } from '../model/custom-http-response';
import { ShowDate } from '../model/show-date';

@Injectable({
  providedIn: 'root'
})
export class ShowDateService {
  public host = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public addNewShowDate(formData: FormData): Observable<ShowDate> {
    return this.http.post<ShowDate>(`${this.host}/show-date/add`, formData);
  }

  public updateShowDate(id: number, formData: FormData): Observable<ShowDate> {
    return this.http.put<ShowDate>(`${this.host}/show-date/update/${id}`, formData);
  }

  public registerShowDate(formData: FormData): Observable<ShowDate> {
    return this.http.put<ShowDate>(`${this.host}/show-date/register`, formData);
  }

  public deregisterShowDate(formData: FormData): Observable<ShowDate> {
    return this.http.put<ShowDate>(`${this.host}/show-date/deregister`, formData);
  }

  public deregisterUserShowDate(formData: FormData): Observable<ShowDate> {
    return this.http.put<ShowDate>(`${this.host}/show-date/deregister-user`, formData);
  }

  public getAllShowDatesByShow(showId: number): Observable<ShowDate[]> {
    return this.http.get<ShowDate[]>(`${this.host}/show-date/list-by-show/${showId}`);
  }

  public getAllOwnShowDates(): Observable<ShowDate[]> {
    return this.http.get<ShowDate[]>(`${this.host}/show-date/list-own`);
  }

  public getAllFreeShowDates(showId: number): Observable<ShowDate[]> {
    return this.http.get<ShowDate[]>(`${this.host}/show-date/list-free/${showId}`);
  }

  public getAllRegisteredShowDates(showId: number): Observable<ShowDate[]> {
    return this.http.get<ShowDate[]>(`${this.host}/show-date/list-registered/${showId}`);
  }

  public deleteShowDate(id: number): Observable<CustomHttpResponse> {
    return this.http.delete<CustomHttpResponse>(`${this.host}/show-date/delete/${id}`);
  }
}
