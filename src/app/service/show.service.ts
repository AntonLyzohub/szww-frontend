import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CustomHttpResponse } from '../model/custom-http-response';
import { Show } from '../model/show';

@Injectable({
  providedIn: 'root'
})
export class ShowService {
  public host = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public addNewShow(formData: FormData): Observable<Show> {
    return this.http.post<Show>(`${this.host}/show/add`, formData);
  }

  public updateShow(id: number, formData: FormData): Observable<Show> {
    return this.http.put<Show>(`${this.host}/show/update/${id}`, formData);
  }

  public getShow(id: number): Observable<Show> {
    return this.http.get<Show>(`${this.host}/show/find/${id}`);
  }

  public getAllShowsByEvent(eventId: number): Observable<Show[]> {
    return this.http.get<Show[]>(`${this.host}/show/list/${eventId}`);
  }

  public deleteShow(id: number): Observable<CustomHttpResponse> {
    return this.http.delete<CustomHttpResponse>(`${this.host}/show/delete/${id}`);
  }
}
