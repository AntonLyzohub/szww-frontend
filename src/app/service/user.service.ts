import { HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CustomHttpResponse } from '../model/custom-http-response';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public host = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public addNewUser(formData: FormData): Observable<User> {
    return this.http.post<User>(`${this.host}/user/add`, formData);
  }

  public updateUser(formData: FormData): Observable<HttpResponse<User>> {
    return this.http.put<User>(`${this.host}/user/update`, formData, { observe: 'response' });
  }

  public toggleUsersLockedStatus(formData: FormData): Observable<User> {
    return this.http.post<User>(`${this.host}/user/locked`, formData);
  }

  public getUser(username: string): Observable<User> {
    return this.http.get<User>(`${this.host}/user/find/${username}`);
  }

  public getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${this.host}/user/list`);
  }

  public getProfileImage(profileImageUrl: string): Observable<any> {
    return this.http.get<any>(profileImageUrl);
  }

  public forgotPassword(formData: FormData): Observable<CustomHttpResponse> {
    return this.http.post<CustomHttpResponse>(`${this.host}/user/forgot-password`, formData);
  }

  public resetPassword(formData: FormData): Observable<CustomHttpResponse> {
    return this.http.post<CustomHttpResponse>(`${this.host}/user/reset-password`, formData);
  }

  public deleteUser(username: string): Observable<CustomHttpResponse> {
    return this.http.delete<CustomHttpResponse>(`${this.host}/user/delete/${username}`);
  }

  public updateProfileImage(formData: FormData): Observable<HttpEvent<User>> {
    return this.http.put<User>(`${this.host}/user/update-profile-image`, formData, 
    {
      reportProgress: true,
      observe: 'events'
    });
  }
}
