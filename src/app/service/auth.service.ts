import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public host = environment.apiUrl;
  private token: string;
  private jwtHelper = new JwtHelperService(); 

  constructor(private http: HttpClient) {}

  public login(formData: FormData): Observable<HttpResponse<User>> {
    return this.http.post<User>(`${this.host}/user/login`, formData, { observe: 'response' });
  }

  public register(formData: FormData): Observable<User> {
    return this.http.post<User>(`${this.host}/user/register`, formData);
  }

  public logOut(): void {
    this.token = null;
    localStorage.removeItem('user');
    localStorage.removeItem('token');
  }

  public saveToken(token: string): void {
    this.token = token;
    localStorage.setItem('token', token);
  }

  public addUserToLocalCache(user: User): void {
    localStorage.setItem('user', JSON.stringify(user));
  }

  public getUserFromLocalCache(): User {
    return JSON.parse(localStorage.getItem('user'));
  }

  public loadToken(): void {
    this.token = localStorage.getItem('token');
  }

  public getToken(): string {
    return this.token;
  }

  public isUserLoggedIn(): boolean {
    this.loadToken();
    if (this.token != null && this.token !== ''){
      if (this.jwtHelper.decodeToken(this.token).sub != null || '') {
        if (!this.jwtHelper.isTokenExpired(this.token)) {
          return true;
        } else {
          this.logOut(); 
          return false;
        }
      } else {
        this.logOut(); 
        return false;
      }
    } else {
      this.logOut(); 
      return false;
    }
  }
}
