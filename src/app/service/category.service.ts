import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Category } from '../model/category';
import { CustomHttpResponse } from '../model/custom-http-response';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  public host = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public addNewCategory(formData: FormData): Observable<Category> {
    return this.http.post<Category>(`${this.host}/category/add`, formData);
  }

  public updateCategories(categories: Category[]): Observable<Category[]> {
    return this.http.put<Category[]>(`${this.host}/category/update`, categories);
  }

  public getCategory(id: number): Observable<Category> {
    return this.http.get<Category>(`${this.host}/category/find/${id}`);
  }

  public getAllCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(`${this.host}/category/list`);
  }

  public deleteCategory(id: number): Observable<CustomHttpResponse> {
    return this.http.delete<CustomHttpResponse>(`${this.host}/category/delete/${id}`);
  }
}
