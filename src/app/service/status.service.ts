import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CustomHttpResponse } from '../model/custom-http-response';
import { Status } from '../model/status';

@Injectable({
  providedIn: 'root'
})
export class StatusService {
  public host = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public addNewStatus(formData: FormData): Observable<Status> {
    return this.http.post<Status>(`${this.host}/status/add`, formData);
  }

  public updateStatuses(statuses: Status[]): Observable<Status[]> {
    return this.http.put<Status[]>(`${this.host}/status/update`, statuses);
  }

  public getStatus(id: number): Observable<Status> {
    return this.http.get<Status>(`${this.host}/status/find/${id}`);
  }

  public getAllStatuses(): Observable<Status[]> {
    return this.http.get<Status[]>(`${this.host}/status/list`);
  }

  public deleteStatus(id: number): Observable<CustomHttpResponse> {
    return this.http.delete<CustomHttpResponse>(`${this.host}/status/delete/${id}`);
  }
}
