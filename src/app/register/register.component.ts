import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from '../model/user';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  public showLoading: boolean;
  private subscriptions: Subscription[] = [];
  public hidePassword: boolean = true;

  public userFormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9 -.ĄąĘęŻżŹźĆćŚśÓóŁł]+$"), 
                              Validators.minLength(3), Validators.maxLength(45)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,12}$")]),
    tel: new FormControl('', [Validators.pattern("^[0-9 -]+$"), Validators.minLength(9), Validators.maxLength(11)]),
    groupSize: new FormControl(1, [Validators.required, Validators.pattern("^[0-9]+$")])
  });

  constructor(private authService: AuthService,
    private router: Router,
    private _snackBar: MatSnackBar) {}

  ngOnInit(): void {
    if (this.authService.isUserLoggedIn()) {
      this.router.navigateByUrl('/events');
    }
  }

  public onRegister(): void {
    this.showLoading = true;
    const formData = new FormData();
    formData.append('username', this.userFormGroup.get("username").value);
    formData.append('email', this.userFormGroup.get("email").value);
    formData.append('password', this.userFormGroup.get("password").value);
    if(this.userFormGroup.get("tel").value == null || undefined)
      formData.append('tel', '');
    else
      formData.append('tel', this.userFormGroup.get("tel").value);
    formData.append('groupSize', this.userFormGroup.get("groupSize").value);

    this.subscriptions.push(
      this.authService.register(formData).subscribe(
        (response: User) => {
          this.showLoading = false;
          this._snackBar.open(`${response.username}, twoje konto zostało utworzone pomyślnie. Teraz możesz się zalogować.`, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
          this.router.navigateByUrl('/login');
        },
        (errorResponse: HttpErrorResponse) => {
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
          this.showLoading = false;
        }
      )
    );
  }

  public toggleHidePassword(): void {
    this.hidePassword = !this.hidePassword;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
