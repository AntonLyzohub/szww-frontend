import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CustomHttpResponse } from '../model/custom-http-response';
import { UserService } from '../service/user.service';

@Component({
  selector: 'reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
  public showLoading: boolean;
  private subscriptions: Subscription[] = [];
  public resetPasswordToken;
  public hidePassword1: boolean = true;
  public hidePassword2: boolean = true;

  public passwordFormGroup = new FormGroup({
    newPassword: new FormControl('', [Validators.required, Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,12}$")]),
    repeatPassword: new FormControl('', [Validators.required, Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,12}$")])
  });
  
  constructor(private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe((params: ParamMap) => {
      const token = params.get('token');
      this.resetPasswordToken = token;
    });
  }

  public get checkPasswordMatch(): boolean {
    const matched: boolean = this.passwordFormGroup.get("newPassword").value === this.passwordFormGroup.get("repeatPassword").value;

    if (matched) {
      this.passwordFormGroup.controls.repeatPassword.setErrors(null);
    } else {
      this.passwordFormGroup.controls.repeatPassword.setErrors({
         notMatched: true
      });
    }
    return matched;
  }

  public onResetPassword(): void {
    this.showLoading = true;
    const formData = new FormData();
    formData.append('token', this.resetPasswordToken);
    formData.append('password', this.passwordFormGroup.get("newPassword").value);
    
    this.subscriptions.push(
      this.userService.resetPassword(formData).subscribe(
        (response: CustomHttpResponse) => {
          this.showLoading = false;
          this._snackBar.open(response.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
          this.router.navigateByUrl('/login');
        },
        (errorResponse: HttpErrorResponse) => {
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
          this.showLoading = false;
        }
      )
    );
  }

  public toggleHidePassword1(): void {
    this.hidePassword1 = !this.hidePassword1;
  }

  public toggleHidePassword2(): void {
    this.hidePassword2 = !this.hidePassword2;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
