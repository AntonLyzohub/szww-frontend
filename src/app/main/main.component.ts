import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AddEventDialogComponent } from '../dialog/add-event-dialog/add-event-dialog.component';
import { EditCategoryDialogComponent } from '../dialog/edit-category-dialog/edit-category-dialog.component';
import { EditStatusDialogComponent } from '../dialog/edit-status-dialog/edit-status-dialog.component';
import { Role } from '../enum/role.enum';
import { Category } from '../model/category';
import { Status } from '../model/status';
import { AuthService } from '../service/auth.service';
import { CategoryService } from '../service/category.service';
import { EventService } from '../service/event.service';
import { StatusService } from '../service/status.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  private subscriptions: Subscription[] = [];
  private filterEvent: Event;

  public dataSource;
  public displayedColumns: string[] = [
    'status', 'title', 'category', 'building', 'startDate', 'endDate'];
  public categories: Category[];
  public selectedCategory = null;
  public statuses: Status[];
  public selectedStatus = null;

  constructor(private router: Router, 
    private authService: AuthService, 
    private eventService: EventService, 
    private categoryService: CategoryService, 
    private statusService: StatusService, 
    private dialog: MatDialog) { }

  ngOnInit(): void {
    this.getEvents();
    this.getCategories();
    this.getStatuses();
  }

  private getEvents(): void {
    this.subscriptions.push(
      this.eventService.getAllEvents(null, null)
      .subscribe(res => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.dataSource.paginator._intl = getPaginatorIntl();
      })
    );
  }

  private getCategories(): void {
    this.subscriptions.push(
      this.categoryService.getAllCategories()
      .subscribe(res => {
        this.categories = res;
      })
    );
  }

  private getStatuses(): void {
    this.subscriptions.push(
      this.statusService.getAllStatuses()
      .subscribe(res => {
        this.statuses = res;
      })
    );
  }

  public applyFilter(event: Event) {
    if(event) {
      this.filterEvent = event;
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
  
      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }
    }
  }

  public onSelectionChange(): void {
    this.subscriptions.push(
      this.eventService.getAllEvents(this.selectedCategory, this.selectedStatus)
      .subscribe(res => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.applyFilter(this.filterEvent); 
      })
    );
  }

  public openAddEventDialog(): void {
    const dialogRef = this.dialog.open(AddEventDialogComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.getEvents();
      }
    });
  }

  public openEditCategoryDialog(): void {
    const dialogRef = this.dialog.open(EditCategoryDialogComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getCategories();
      if(result)
        this.getEvents();
    });
  }

  public openEditStatusDialog(): void {
    const dialogRef = this.dialog.open(EditStatusDialogComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getStatuses();
      if(result)
        this.getEvents();
    });
  }

  public navigateToEvent(eventId: number): void {
    this.router.navigate(['/event/', eventId]);
  }

  public get isAdmin(): boolean {
    return this.authService.getUserFromLocalCache()?.role === Role.ADMIN;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}

// Translate mat-paginator lables to Polish
export function getPaginatorIntl(): MatPaginatorIntl {
  const paginatorIntl: MatPaginatorIntl = new MatPaginatorIntl();

  const rangeLabel: string = 'z';
  const itemsPerPageLabel: string = 'Liczba wierszy na stronie:';
  const firstPageLabel: string = 'Pierwsza strona';
  const lastPageLabel: string = 'Ostatnia strona';
  const previousPageLabel: string = 'Poprzednia strona';
  const nextPageLabel: string = 'Następna strona';

  const getRangeLabel: (page: number, pageSize: number, length: number) => string = (
    page: number,
    pageSize: number,
    length: number
  ): string => {
    return new MatPaginatorIntl().getRangeLabel(page, pageSize, length).replace(/[a-z]+/i, rangeLabel);
  };

  paginatorIntl.itemsPerPageLabel = itemsPerPageLabel;
  paginatorIntl.firstPageLabel = firstPageLabel;
  paginatorIntl.lastPageLabel = lastPageLabel;
  paginatorIntl.previousPageLabel = previousPageLabel;
  paginatorIntl.nextPageLabel = nextPageLabel;
  paginatorIntl.getRangeLabel = getRangeLabel;
  
  return paginatorIntl;
}
