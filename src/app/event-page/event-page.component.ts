import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatAccordion } from '@angular/material/expansion';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AddShowDateDialogComponent } from '../dialog/add-show-date-dialog/add-show-date-dialog.component';
import { AddShowDialogComponent } from '../dialog/add-show-dialog/add-show-dialog.component';
import { DeleteEventWarningDialogComponent } from '../dialog/delete-event-warning-dialog/delete-event-warning-dialog.component';
import { DeleteShowWarningDialogComponent } from '../dialog/delete-show-warning-dialog/delete-show-warning-dialog.component';
import { EditEventDialogComponent } from '../dialog/edit-event-dialog/edit-event-dialog.component';
import { EditShowDialogComponent } from '../dialog/edit-show-dialog/edit-show-dialog.component';
import { RegisterShowDialogComponent } from '../dialog/register-show-dialog/register-show-dialog.component';
import { Role } from '../enum/role.enum';
import { Event } from '../model/event';
import { Show } from '../model/show';
import { AuthService } from '../service/auth.service';
import { EventService } from '../service/event.service';
import { ShowService } from '../service/show.service';
import { registerLocaleData } from '@angular/common';
import localePl from '@angular/common/locales/pl';
import { LOCALE_ID } from '@angular/core';

@Component({
  selector: 'app-event-page',
  templateUrl: './event-page.component.html',
  styleUrls: ['./event-page.component.css'],
  providers: [{ provide: LOCALE_ID, useValue: "pl-PL" }]
})
export class EventPageComponent implements OnInit, OnDestroy {
  @ViewChild(MatAccordion) accordion: MatAccordion;
  private subscriptions: Subscription[] = [];
  public event: Event;
  public shows: Show[];
  public readonly finishedStatusId = 13;

  constructor(private route: ActivatedRoute,
    private eventService: EventService,
    private showService: ShowService,
    private authService: AuthService,
    private dialog: MatDialog,
    private router: Router,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    registerLocaleData(localePl);
    this.route.paramMap.subscribe((params: ParamMap) => {
      const eventId = +params.get('id');
      this.getEvent(eventId);
      this.getShows(eventId);
    });
  }

  public getEvent(id: number): void {
    this.subscriptions.push(
      this.eventService.getEvent(id).subscribe(
        (response: Event) => {
          this.event = response;
        },
        (errorResponse: HttpErrorResponse) => {
          this.router.navigate(['/events/']);
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  public getShows(eventId: number): void {
    this.subscriptions.push(
      this.showService.getAllShowsByEvent(eventId).subscribe(shows => {
        this.shows = shows;
      })
    );
  }

  public openRegisterShowDialog(show: Show): void {
    this.dialog.open(RegisterShowDialogComponent, {
      width: '400px',
      data: show
    });
  }

  public openAddShowDateDialog(show: Show): void {
    this.dialog.open(AddShowDateDialogComponent, {
      width: '660px',
      data: show
    });
  }

  public openEditShowDialog(show: Show): void {
    this.dialog.open(EditShowDialogComponent, {
      width: '420px',
      data: show
    });
  }

  public openDeleteShowWarningDialog(show: Show): void {
    const dialogRef = this.dialog.open(DeleteShowWarningDialogComponent, {
      width: '380px',
      data: show
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.getShows(this.event.id);
      }
    });
  }

  public openAddShowDialog(): void {
    const dialogRef = this.dialog.open(AddShowDialogComponent, {
      width: '420px',
      data: this.event
    });

    dialogRef.afterClosed().subscribe(newShow => {
      if(newShow) {
        this.shows.push(newShow);
      }
    });
  }

  public openEditEventDialog(): void {
    const dialogRef = this.dialog.open(EditEventDialogComponent, {
      width: '500px',
      data: this.event
    });

    dialogRef.afterClosed().subscribe(updatedEvent => {
      if(updatedEvent) {
        this.event = updatedEvent;
        this.getShows(updatedEvent.id);
      }
    });
  }

  public openDeleteEventWarningDialog(): void {
    this.dialog.open(DeleteEventWarningDialogComponent, {
      width: '305px',
      data: this.event
    });
  }

  public get isUserLoggedIn(): boolean {
    return this.authService.isUserLoggedIn();
  }

  public get isAdmin(): boolean {
    return this.authService.getUserFromLocalCache()?.role === Role.ADMIN;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
