import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { BuildingRoomList } from 'src/app/enum/building-room-list';
import { professorList } from 'src/app/enum/professor-list';
import { Show } from 'src/app/model/show';
import { ShowService } from 'src/app/service/show.service';

@Component({
  selector: 'edit-show-dialog',
  templateUrl: './edit-show-dialog.component.html',
  styleUrls: ['./edit-show-dialog.component.css']
})
export class EditShowDialogComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public showLoading: boolean;

  public name: string;
  public description: string;
  public professor: string;
  public room: string;

  public rooms;
  public professors = professorList;
  private BuildingRoomList = BuildingRoomList;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Show, 
    public dialogRef: MatDialogRef<EditShowDialogComponent>,
    private _snackBar: MatSnackBar, 
    private showService: ShowService) { }

  ngOnInit(): void {
    this.name = this.data.name; 
    this.description = this.data.description;
    this.professor = this.data.professor;
    this.room = this.data.room;
    this.BuildingRoomList.forEach(e => {
      if(e.building == this.data.event.building)
        this.rooms = e.rooms;
    });
  }

  public onSubmit(): void {
    this.showLoading = true;
    const formData = new FormData();
    formData.append('name', this.name);
    formData.append('professor', this.professor);
    formData.append('room', this.room);
    formData.append('eventId', JSON.stringify(this.data.event.id));
    if(this.description == null || this.description == undefined || this.description == '')
      formData.append('description', 'Brak');
    else
      formData.append('description', this.description);

    this.subscriptions.push(
      this.showService.updateShow(this.data.id, formData).subscribe(
        (response: Show) => {
          this.showLoading = false;
          this.data.name = response.name;
          this.data.description = response.description;
          this.data.professor = response.professor;
          this.data.room = response.room;
          this.dialogRef.close();
          this._snackBar.open(`Zmiany zapisane pomyślnie`, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        },
        (errorResponse: HttpErrorResponse) => {
          this.showLoading = false;
          this.dialogRef.close();
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
