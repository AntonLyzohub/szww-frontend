import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CustomHttpResponse } from 'src/app/model/custom-http-response';
import { Event } from 'src/app/model/event';
import { EventService } from 'src/app/service/event.service';

@Component({
  selector: 'delete-event-warning-dialog',
  templateUrl: './delete-event-warning-dialog.component.html',
  styleUrls: ['./delete-event-warning-dialog.component.css']
})
export class DeleteEventWarningDialogComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public showLoading: boolean;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Event,
    public dialogRef: MatDialogRef<DeleteEventWarningDialogComponent>,
    private _snackBar: MatSnackBar, 
    private eventService: EventService, 
    private router: Router) {}

  ngOnInit(): void {
  }

  public onDelete(): void {
    this.showLoading = true;
    this.subscriptions.push(
      this.eventService.deleteEvent(this.data.id).subscribe(
        (response: CustomHttpResponse) => {
          this.showLoading = false;
          this.dialogRef.close();
          this.router.navigate(['/events/']);
          this._snackBar.open(response.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        },
        (errorResponse: HttpErrorResponse) => {
          this.showLoading = false; 
          this.dialogRef.close();
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
