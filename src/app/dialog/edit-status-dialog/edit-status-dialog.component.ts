import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { Status } from 'src/app/model/status';
import { StatusService } from 'src/app/service/status.service';

@Component({
  selector: 'edit-status-dialog',
  templateUrl: './edit-status-dialog.component.html',
  styleUrls: ['./edit-status-dialog.component.css']
})
export class EditStatusDialogComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public showLoading: boolean;

  public statuses: Status[];
  private changedStatusIds: number[] = [];

  constructor(public dialogRef: MatDialogRef<EditStatusDialogComponent>,
    private _snackBar: MatSnackBar, 
    private statusService: StatusService) { }

  ngOnInit(): void {
    this.getAllStatuses();
  }

  public onStatusChange(status: Status): void {
    this.changedStatusIds.push(status.id);
  }

  public onSubmit(): void {
    this.showLoading = true;
    const unique = this.changedStatusIds.filter(function(elem, index, self) {
      return index === self.indexOf(elem);
    });
    let changedStatuses: Status[] = []; 
    unique.forEach(statusId => {
      const changedStatus = this.statuses.find(s => s.id == statusId);
      changedStatuses.push(changedStatus);
    });
    
    this.subscriptions.push(
      this.statusService.updateStatuses(changedStatuses).subscribe(
        (response: Status[]) => {
          this.showLoading = false;
          this.dialogRef.close(true);
          this._snackBar.open("Zmiany zapisane pomyślnie", "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        },
        (errorResponse: HttpErrorResponse) => {
          this.showLoading = false;
          this.dialogRef.close(false);
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  private getAllStatuses(): void {
    this.subscriptions.push(
      this.statusService.getAllStatuses().subscribe(
        (response: Status[]) => {
          this.statuses = response;
        },
        (errorResponse: HttpErrorResponse) => {
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
