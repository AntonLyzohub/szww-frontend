import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { CustomHttpResponse } from 'src/app/model/custom-http-response';
import { Show } from 'src/app/model/show';
import { ShowDate } from 'src/app/model/show-date';
import { ShowDateService } from 'src/app/service/show-date.service';
import { registerLocaleData } from '@angular/common';
import localePl from '@angular/common/locales/pl';
import { LOCALE_ID } from '@angular/core';
import { UpdateShowDateDialogComponent } from '../update-show-date-dialog/update-show-date-dialog.component';
import { User } from 'src/app/model/user';
import { UserDialogComponent } from '../user-dialog/user-dialog.component';

@Component({
  selector: 'add-show-date-dialog',
  templateUrl: './add-show-date-dialog.component.html',
  styleUrls: ['./add-show-date-dialog.component.css'],
  providers: [{ provide: LOCALE_ID, useValue: "pl-PL" }]
})
export class AddShowDateDialogComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public showLoading: boolean;

  public minDate: Date;
  public maxDate: Date;
  public dateValue: Date;
  public timeFrom: string;
  public timeTo: string;
  public showDates: ShowDate[];
  public timeList = ['00:00', '00:30', 
                     '01:00', '01:30', 
                     '02:00', '02:30',  
                     '03:00', '03:30', 
                     '04:00', '04:30', 
                     '05:00', '05:30', 
                     '06:00', '06:15', '06:30', '06:45', 
                     '07:00', '07:15', '07:30', '07:45', 
                     '08:00', '08:15', '08:30', '08:45', 
                     '09:00', '09:15', '09:30', '09:45', 
                     '10:00', '10:15', '10:30', '10:45', 
                     '11:00', '11:15', '11:30', '11:45', 
                     '12:00', '12:15', '12:30', '12:45', 
                     '13:00', '13:15', '13:30', '13:45', 
                     '14:00', '14:15', '14:30', '14:45', 
                     '15:00', '15:15', '15:30', '15:45', 
                     '16:00', '16:15', '16:30', '16:45', 
                     '17:00', '17:15', '17:30', '17:45', 
                     '18:00', '18:15', '18:30', '18:45', 
                     '19:00', '19:15', '19:30', '19:45', 
                     '20:00', '20:15', '20:30', '20:45', 
                     '21:00', '21:15', '21:30', '21:45', 
                     '22:00', '22:15', '22:30', '22:45', 
                     '23:00', '23:15', '23:30', '23:45'];

  constructor(@Inject(MAT_DIALOG_DATA) public data: Show, 
    public dialogRef: MatDialogRef<AddShowDateDialogComponent>,
    private _snackBar: MatSnackBar, 
    private dialog: MatDialog, 
    private showDateService: ShowDateService) { }
 
  ngOnInit(): void {
    registerLocaleData(localePl);
    this.getAllShowDates();
    this.minDate = new Date(this.data.event.startDate);
    this.maxDate = new Date(this.data.event.endDate);
    this.maxDate.setHours(23);
  }

  private getAllShowDates(): void {
    this.subscriptions.push(
      this.showDateService.getAllShowDatesByShow(this.data.id).subscribe(
        (response: ShowDate[]) => {
          this.showDates = response;
        },
        (errorResponse: HttpErrorResponse) => {
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  public onShowDateAdd(): void {
    this.showLoading = true;
    const date = Date.UTC(this.dateValue.getFullYear(), 
                          this.dateValue.getMonth(), 
                          this.dateValue.getDate(), 23, 59);

    const formData = new FormData();
    formData.append('date', new Date(date).toISOString());
    formData.append('timeFrom', this.timeFrom);
    formData.append('timeTo', this.timeTo);
    formData.append('showId', JSON.stringify(this.data.id));

    this.subscriptions.push(
      this.showDateService.addNewShowDate(formData).subscribe(
        (response: ShowDate) => {
          this.showLoading = false;
          this.getAllShowDates();
          this._snackBar.open(`Nowy termin został zapisany pomyślnie`, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        },
        (errorResponse: HttpErrorResponse) => {
          this.showLoading = false;
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  public onShowDateDeregister(showDate: ShowDate): void {
    const formData = new FormData();
    formData.append('id', JSON.stringify(showDate.id));

    this.subscriptions.push(
      this.showDateService.deregisterUserShowDate(formData).subscribe(
        (response: ShowDate) => {
          var index = this.showDates.findIndex(showDate => showDate.id == response.id)
          this.showDates[index] = response;
          this.getAllShowDates();
        },
        (errorResponse: HttpErrorResponse) => {
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  public onShowDateDelete(showDate: ShowDate): void {
    this.subscriptions.push(
      this.showDateService.deleteShowDate(showDate.id).subscribe(
        (response: CustomHttpResponse) => {
          this.getAllShowDates();
          this._snackBar.open(response.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        },
        (errorResponse: HttpErrorResponse) => {
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  public openUpdateShowDateDialog(showDate: ShowDate) {
    const dialogRef = this.dialog.open(UpdateShowDateDialogComponent, {
      width: '500px',
      data: showDate
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.getAllShowDates();
      }
    });
  }

  public openUserDialog(user: User) {
    this.dialog.open(UserDialogComponent, {
      width: '500px',
      data: user
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
