import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { BuildingRoomList } from 'src/app/enum/building-room-list';
import { professorList } from 'src/app/enum/professor-list';
import { Event } from 'src/app/model/event';
import { Show } from 'src/app/model/show';
import { ShowService } from 'src/app/service/show.service';

@Component({
  selector: 'add-show-dialog',
  templateUrl: './add-show-dialog.component.html',
  styleUrls: ['./add-show-dialog.component.css']
})
export class AddShowDialogComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public showLoading: boolean;

  public name: string;
  public description: string;
  public professor: string;
  public room: string;

  public rooms;
  public professors = professorList;
  private BuildingRoomList = BuildingRoomList;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Event, 
    public dialogRef: MatDialogRef<AddShowDialogComponent>,
    private _snackBar: MatSnackBar, 
    private showService: ShowService) { }

  ngOnInit(): void {
    this.BuildingRoomList.forEach(e => {
      if(e.building == this.data.building)
        this.rooms = e.rooms;
    });
  }

  public onSubmit(): void {
    this.showLoading = true;
    const formData = new FormData();
    formData.append('name', this.name);
    formData.append('professor', this.professor);
    formData.append('room', this.room);
    formData.append('eventId', JSON.stringify(this.data.id));
    if(this.description == null || this.description == undefined || this.description == '')
      formData.append('description', 'Brak');
    else
      formData.append('description', this.description);

    this.subscriptions.push(
      this.showService.addNewShow(formData).subscribe(
        (response: Show) => {
          this.showLoading = false;
          this.dialogRef.close(response);
          this._snackBar.open(`Nowy pokaz został utworzony pomyślnie`, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        },
        (errorResponse: HttpErrorResponse) => {
          this.showLoading = false;
          this.dialogRef.close();
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
