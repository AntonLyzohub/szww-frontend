import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.css']
})
export class UserDialogComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public showLoading: boolean;

  constructor(@Inject(MAT_DIALOG_DATA) public data: User,
    private userService: UserService,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  public toggleUsersLockedStatus(): void {
    this.showLoading = true;
    const formData = new FormData();
    formData.append('username', this.data.username);

    this.subscriptions.push(
      this.userService.toggleUsersLockedStatus(formData).subscribe(
        (response: User) => {
          this.showLoading = false;
          this.data = response;
          if(response.notLocked == true) {
            this._snackBar.open(`Konto użytkownika zostało odblokowane`, "OK", {
              horizontalPosition: "start",
              verticalPosition: "bottom",
              panelClass: ['mysnackbar'],
              duration: 5000
            });
          } else {
            this._snackBar.open(`Konto użytkownika zostało zablokowane`, "OK", {
              horizontalPosition: "start",
              verticalPosition: "bottom",
              panelClass: ['mysnackbar'],
              duration: 5000
            });
          }
        },
        (errorResponse: HttpErrorResponse) => {
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
          this.showLoading = false;
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
