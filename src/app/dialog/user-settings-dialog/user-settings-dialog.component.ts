import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { HeaderType } from 'src/app/enum/header-type.enum';
import { User } from 'src/app/model/user';
import { AuthService } from 'src/app/service/auth.service';
import { UserService } from 'src/app/service/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'user-settings-dialog',
  templateUrl: './user-settings-dialog.component.html',
  styleUrls: ['./user-settings-dialog.component.css']
})
export class UserSettingsDialogComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public showLoading: boolean;
  
  private profileImage: File;
  public profileImageUrl;
  public userFormGroup = new FormGroup({
    username: new FormControl('', [
      Validators.required, 
      Validators.pattern("^[a-zA-Z0-9 -.ĄąĘęŻżŹźĆćŚśÓóŁł]+$"), 
      Validators.minLength(3), 
      Validators.maxLength(45)
    ]),
    email: new FormControl('', [
      Validators.required, 
      Validators.email
    ]),
    tel: new FormControl('', [
      Validators.pattern("^[0-9 -.]+$"), 
      Validators.minLength(9), 
      Validators.maxLength(11)
    ]),
    groupSize: new FormControl(null, [
      Validators.required, 
      Validators.pattern("^[0-9]+$")
    ])
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: User, 
    public dialogRef: MatDialogRef<UserSettingsDialogComponent>,
    private _snackBar: MatSnackBar, 
    private userService: UserService, 
    private authService: AuthService) { }

  ngOnInit(): void {
    this.profileImageUrl = this.data.profileImageUrl;
    this.userFormGroup.get("username").setValue(this.data.username);
    this.userFormGroup.get("email").setValue(this.data.email);
    this.userFormGroup.get("tel").setValue(this.data.tel);
    this.userFormGroup.get("groupSize").setValue(this.data.groupSize);
    this.getProfileImage();
  }

  private getProfileImage() {
    if(this.data.profileImageUrl.startsWith(`${environment.apiUrl}/user/image`)) {
      fetch(this.data.profileImageUrl)
      .then((res) => {
        return res.blob()
      })
      .then((blob) => {
        this.profileImage = blob as File;
      })
    } else {
      this.profileImage = null;
    }
  }

  public onSubmit(): void {
    this.showLoading = true;
    const formData = new FormData();
    formData.append('username', this.userFormGroup.get("username").value);
    formData.append('email', this.userFormGroup.get("email").value);
    formData.append('groupSize', this.userFormGroup.get("groupSize").value);
    if(this.userFormGroup.get("tel").value == null || undefined)
      formData.append('tel', '');
    else
      formData.append('tel', this.userFormGroup.get("tel").value);
    if(this.profileImage != null)
      formData.append('profileImage', this.profileImage);

    this.subscriptions.push(
      this.userService.updateUser(formData).subscribe(
        (response: HttpResponse<User>) => {
          this.showLoading = false;
          const token = response.headers.get(HeaderType.JWT_TOKEN);
          this.authService.saveToken(token);

          // it refreshes the profile-image in navbar
          response.body.profileImageUrl += "?" +new Date().getTime();
          this.authService.addUserToLocalCache(response.body);
          this.dialogRef.close();
          this._snackBar.open(`Zmiany zapisane pomyślnie`, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        },
        (errorResponse: HttpErrorResponse) => {
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
          this.showLoading = false;
        }
      )
    );
  }

  public onFileSelected(event): void {
    const file: File = event.target.files[0];
    if(file) {
      this.profileImage = file;
      var reader = new FileReader();
      reader.readAsDataURL(file);  // read file as data url
      reader.onload = (event) => {  // called once readAsDataURL is completed
        this.profileImageUrl = event.target.result;
      }
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
