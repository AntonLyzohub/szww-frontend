import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { Category } from 'src/app/model/category';
import { CustomHttpResponse } from 'src/app/model/custom-http-response';
import { CategoryService } from 'src/app/service/category.service';

@Component({
  selector: 'edit-category-dialog',
  templateUrl: './edit-category-dialog.component.html',
  styleUrls: ['./edit-category-dialog.component.css']
})
export class EditCategoryDialogComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public showLoading: boolean;
  public showLoading2: boolean;

  public categories: Category[];
  public newCategory: string;
  private changedCategoryIds: number[] = [];
  private result: boolean = false;

  constructor(public dialogRef: MatDialogRef<EditCategoryDialogComponent>,
    private _snackBar: MatSnackBar, 
    private categoryService: CategoryService) { }

  ngOnInit(): void {
    this.getAllCategories();
  }

  public onCategoryAdd(): void {
    this.showLoading = true;
    const formData = new FormData();
    formData.append('name', this.newCategory);

    this.subscriptions.push(
      this.categoryService.addNewCategory(formData).subscribe(
        (response: Category) => {
          this.categories.push(response);
          this.newCategory = "";
          this.result = true;
          this.showLoading = false;
          this._snackBar.open(`Nowa kategoria została utworzona`, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        },
        (errorResponse: HttpErrorResponse) => {
          this.showLoading = false;
          this._snackBar.open(errorResponse.error.message, null, {
            duration: 4000,
          });
        }
      )
    );
  }

  public onCategoryDelete(category: Category): void {
    this.subscriptions.push(
      this.categoryService.deleteCategory(category.id).subscribe(
        (response: CustomHttpResponse) => {
          this.getAllCategories();
          this.result = true;
          this._snackBar.open(`Kategoria została usunięta pomyślnie`, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        },
        (errorResponse: HttpErrorResponse) => {
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  public onCategoryChange(category: Category): void {
    this.changedCategoryIds.push(category.id);
  }

  public onSubmit(): void {
    this.showLoading2 = true;
    const unique = this.changedCategoryIds.filter(function(elem, index, self) {
      return index === self.indexOf(elem);
    });
    let changedCategories: Category[] = []; 
    unique.forEach(categoryId => {
      const changedCategory = this.categories.find(c => c.id == categoryId);
      changedCategories.push(changedCategory);
    });
    this.subscriptions.push(
      this.categoryService.updateCategories(changedCategories).subscribe(
        (response: Category[]) => {
          this.showLoading2 = false;
          this.result = true;
          this.dialogRef.close(this.result);
          this._snackBar.open("Zmiany zapisane pomyślnie", "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        },
        (errorResponse: HttpErrorResponse) => {
          this.showLoading2 = false;
          this.dialogRef.close(this.result);
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  private getAllCategories(): void {
    this.subscriptions.push(
      this.categoryService.getAllCategories().subscribe(
        (response: Category[]) => {
          this.categories = response;
        },
        (errorResponse: HttpErrorResponse) => {
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
