import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { CustomHttpResponse } from 'src/app/model/custom-http-response';
import { Show } from 'src/app/model/show';
import { ShowService } from 'src/app/service/show.service';

@Component({
  selector: 'delete-show-warning-dialog',
  templateUrl: './delete-show-warning-dialog.component.html',
  styleUrls: ['./delete-show-warning-dialog.component.css']
})
export class DeleteShowWarningDialogComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public showLoading: boolean;
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: Show, 
    public dialogRef: MatDialogRef<DeleteShowWarningDialogComponent>,
    private _snackBar: MatSnackBar, 
    private showService: ShowService) { }

  ngOnInit(): void {
  }

  public onDelete(): void {
    this.showLoading = true;
    this.subscriptions.push(
      this.showService.deleteShow(this.data.id).subscribe(
        (response: CustomHttpResponse) => {
          this.showLoading = false;
          this.dialogRef.close(true);
          this._snackBar.open(`Pokaz został usunięty pomyślnie`, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        },
        (errorResponse: HttpErrorResponse) => {
          this.showLoading = false;
          this.dialogRef.close(false);
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
