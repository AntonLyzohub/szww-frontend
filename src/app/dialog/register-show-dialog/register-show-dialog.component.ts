import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { Show } from 'src/app/model/show';
import { ShowDate } from 'src/app/model/show-date';
import { ShowDateService } from 'src/app/service/show-date.service';
import { registerLocaleData } from '@angular/common';
import localePl from '@angular/common/locales/pl';
import { LOCALE_ID } from '@angular/core';

@Component({
  selector: 'register-show-dialog',
  templateUrl: './register-show-dialog.component.html',
  styleUrls: ['./register-show-dialog.component.css'],
  providers: [{ provide: LOCALE_ID, useValue: "pl-PL" }]
})
export class RegisterShowDialogComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public showLoading: boolean;

  public showDates: ShowDate[] = [];
  public selectedShowDateId: number = null;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Show, 
    public dialogRef: MatDialogRef<RegisterShowDialogComponent>,
    private _snackBar: MatSnackBar,
    private showDateService: ShowDateService) {}

  ngOnInit() {
    registerLocaleData(localePl);
    this.getFreeShowDates();
  }

  private getFreeShowDates(): void {
    this.subscriptions.push(
      this.showDateService.getAllFreeShowDates(this.data.id).subscribe(showDates => {
        this.showDates = showDates;
      })
    );
  }

  public reserveShowDate(): void {
    this.showLoading = true;
    const formData = new FormData();
    formData.append('id', JSON.stringify(this.selectedShowDateId));
    
    this.subscriptions.push(
      this.showDateService.registerShowDate(formData).subscribe(
        (response: ShowDate) => {
          this.dialogRef.close();
          this._snackBar.open(response.user.username +', rejestracja na pokaz "' +response.show.name +'" wykonana pomyślnie.' , 'OK', {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
        });
        this.showLoading = false;
        },
        (errorResponse: HttpErrorResponse) => {
          this.dialogRef.close();
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
          this.showLoading = false;
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
