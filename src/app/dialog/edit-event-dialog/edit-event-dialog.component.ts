import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { BuildingRoomList } from 'src/app/enum/building-room-list';
import { Category } from 'src/app/model/category';
import { Event } from 'src/app/model/event';
import { CategoryService } from 'src/app/service/category.service';
import { EventService } from 'src/app/service/event.service';

@Component({
  selector: 'edit-event-dialog',
  templateUrl: './edit-event-dialog.component.html',
  styleUrls: ['./edit-event-dialog.component.css']
})
export class EditEventDialogComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public showLoading: boolean;

  public title: string;
  public description: string;
  public startDate: Date;
  public endDate: Date;
  public building: string;
  public categoryId: number;

  public categories: Category[];
  public today = new Date();
  public BuildingRoomList = BuildingRoomList;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Event, 
    public dialogRef: MatDialogRef<EditEventDialogComponent>,
    private _snackBar: MatSnackBar, 
    private eventService: EventService, 
    private categoryService: CategoryService) { }

  ngOnInit(): void {
    this.title = this.data.title;
    this.description = this.data.description;
    this.startDate = new Date(this.data.startDate);
    this.endDate = new Date(this.data.endDate);
    this.building = this.data.building;
    this.categoryId = this.data.category.id;
    this.getAllCategories();
  }

  public onSubmit(): void {
    this.showLoading = true;
    this.startDate.setHours(1);
    this.endDate.setHours(23);
    this.endDate.setMinutes(59);
    const formData = new FormData();
    formData.append('title', this.title);
    formData.append('description', this.description);
    formData.append('startDate', this.startDate.toISOString());
    formData.append('endDate', this.endDate.toISOString());
    formData.append('building', this.building);
    formData.append('categoryId', JSON.stringify(this.categoryId));

    this.subscriptions.push(
      this.eventService.updateEvent(this.data.id, formData).subscribe(
        (response: Event) => {
          this.dialogRef.close(response);
          this._snackBar.open(`Zmiany zapisane pomyślnie`, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
          this.showLoading = false;
        },
        (errorResponse: HttpErrorResponse) => {
          this.dialogRef.close();
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
          this.showLoading = false;
        }
      )
    );
  }

  private getAllCategories(): void {
    this.subscriptions.push(
      this.categoryService.getAllCategories().subscribe(
        (response: Category[]) => {
          this.categories = response;
        },
        (errorResponse: HttpErrorResponse) => {
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
