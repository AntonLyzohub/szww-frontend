import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { HeaderType } from '../enum/header-type.enum';
import { User } from '../model/user';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  public showLoading: boolean;
  private subscriptions: Subscription[] = [];
  public hidePassword: boolean = true;

  public userFormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.maxLength(45)]),
    password: new FormControl('', [Validators.required])
  });

  constructor(private authService: AuthService,
    private router: Router,
    private _snackBar: MatSnackBar) {}

  ngOnInit(): void {
    if (this.authService.isUserLoggedIn()) {
      this.router.navigateByUrl('/events');
    }
  }

  public onLogin(): void {
    this.showLoading = true;
    const formData = new FormData();
    formData.append('username', this.userFormGroup.get("username").value);
    formData.append('password', this.userFormGroup.get("password").value);
    
    this.subscriptions.push(
      this.authService.login(formData).subscribe(
        (response: HttpResponse<User>) => {
          const token = response.headers.get(HeaderType.JWT_TOKEN);
          this.authService.saveToken(token);
          this.authService.addUserToLocalCache(response.body);
          this.router.navigateByUrl('/events');
          this.showLoading = false;
        },
        (errorResponse: HttpErrorResponse) => {
          this._snackBar.open(errorResponse.error.message, "OK", {
            horizontalPosition: "start",
            verticalPosition: "bottom",
            panelClass: ['mysnackbar'],
            duration: 5000
          });
          this.showLoading = false;
        }
      )
    );
  }

  public toggleHidePassword(): void {
    this.hidePassword = !this.hidePassword;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
