export interface User {
    username: string;
    email: string;
    tel: string;
    groupSize: number;
    profileImageUrl: string;
    notLocked: boolean;
    role: string;
    authorities: [];
}