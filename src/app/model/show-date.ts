import { Show } from "./show";
import { User } from "./user";

export interface ShowDate {
    id: number;
    date: Date;
    timeFrom: string;
    timeTo: string;
    user: User;
    show: Show;
}