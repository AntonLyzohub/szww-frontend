import { Event } from "./event";

export interface Show {
    id: number;
    name: string;
    description: string;
    professor: string;
    room: string;
    event: Event;
}