import { Category } from "./category";
import { Status } from "./status";

export interface Event {
    id: number;
    title: string;
    description: string;
    startDate: Date;
    endDate: Date;
    building: string;
    category: Category;
    status: Status;
}