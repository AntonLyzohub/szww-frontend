import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { CdkTableModule } from '@angular/cdk/table';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MAT_DATE_LOCALE } from '@angular/material/core';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { MyShowsComponent } from './my-shows/my-shows.component';
import { EventPageComponent } from './event-page/event-page.component';
import { RegisterShowDialogComponent } from './dialog/register-show-dialog/register-show-dialog.component';
import { AuthService } from './service/auth.service';
import { AuthInterceptor } from './interceptor/auth.interceptor';
import { UserSettingsDialogComponent } from './dialog/user-settings-dialog/user-settings-dialog.component';
import { EditEventDialogComponent } from './dialog/edit-event-dialog/edit-event-dialog.component';
import { EditShowDialogComponent } from './dialog/edit-show-dialog/edit-show-dialog.component';
import { AddShowDateDialogComponent } from './dialog/add-show-date-dialog/add-show-date-dialog.component';
import { DeleteEventWarningDialogComponent } from './dialog/delete-event-warning-dialog/delete-event-warning-dialog.component';
import { DeleteShowWarningDialogComponent } from './dialog/delete-show-warning-dialog/delete-show-warning-dialog.component';
import { AddShowDialogComponent } from './dialog/add-show-dialog/add-show-dialog.component';
import { AddEventDialogComponent } from './dialog/add-event-dialog/add-event-dialog.component';
import { EditCategoryDialogComponent } from './dialog/edit-category-dialog/edit-category-dialog.component';
import { EditStatusDialogComponent } from './dialog/edit-status-dialog/edit-status-dialog.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { UpdateShowDateDialogComponent } from './dialog/update-show-date-dialog/update-show-date-dialog.component';
import { UserDialogComponent } from './dialog/user-dialog/user-dialog.component';

import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { ColorPickerModule } from 'ngx-color-picker';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DateTimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';

@NgModule({ 
  declarations: [
    AppComponent,
    MainComponent,
    NavbarComponent,
    FooterComponent,
    MyShowsComponent,
    EventPageComponent,
    LoginComponent,
    RegisterComponent,
    RegisterShowDialogComponent,
    UserSettingsDialogComponent,
    EditEventDialogComponent,
    EditShowDialogComponent,
    AddShowDateDialogComponent,
    DeleteEventWarningDialogComponent,
    DeleteShowWarningDialogComponent,
    AddShowDialogComponent,
    AddEventDialogComponent,
    EditCategoryDialogComponent,
    EditStatusDialogComponent,
    ResetPasswordComponent,
    ForgotPasswordComponent,
    UpdateShowDateDialogComponent,
    UserDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatSortModule,
    MatDialogModule, 
    CdkTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatMenuModule,
    MatFormFieldModule,
    MatProgressBarModule,
    ColorPickerModule,
    MatTooltipModule,
    DateTimePickerModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatCheckboxModule
  ],
  exports: [
    MatTableModule,
    MatFormFieldModule
  ],
  providers: [
    DatePipe,
    AuthService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: MAT_DATE_LOCALE, useValue: 'pl-PL' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
