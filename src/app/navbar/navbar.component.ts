import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserSettingsDialogComponent } from '../dialog/user-settings-dialog/user-settings-dialog.component';
import { Role } from '../enum/role.enum';
import { User } from '../model/user';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private authService: AuthService, 
    private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  public get isUserLoggedIn(): boolean {
    return this.authService.isUserLoggedIn();
  }

  public get isAdmin(): boolean {
    return this.authService.getUserFromLocalCache()?.role === Role.ADMIN;
  }

  public get getUser(): User {
    return this.authService.getUserFromLocalCache();
  }

  public openUserSettingsDialog() {
    this.dialog.open(UserSettingsDialogComponent, {
      width: '450px',
      data: this.getUser
    });
  }

  public logout(): void {
    this.authService.logOut();
  }
}
