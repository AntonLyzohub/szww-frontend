export const professorList = [
    'dr inż. Łukasz Hładowski',
    'doc. dr inż. Emil Michta',
    'dr hab. Inż. Paweł Szcześniak',
    'dr hab. inż. Artur Gramacki',
    'dr inż. Jacek Bieganowski',
    'dr inż. Robert Szulim',
    'dr inż. Jacek Tkacz',
    'dr inż. Andrzej Czajkowski',
    'dr inż. Piotr Witczak',
];